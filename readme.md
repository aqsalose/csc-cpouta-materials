# Readme

This repository contains my notes (hopefully in presentable format) for setting up and using cPouta virtual machines (from CSC).

## 0. Warning

cPouta VMs are not confidential enough for **sensitive data**, such as Finrisk2002 population cohort. I hope that we could set up a safe ePouta system for handling them

## 1. Obtaining cPouta access

TL;DR: CSC official instructions are quite good

1. Create CSC account
2. ask PI (in our case, Leo or maybe Teemu) to apply for resources from CSC. Basic scientific use for researchers in Finnish institution is free, but requires application.
3. Follow CSC instructions, they are very good: https://docs.csc.fi/cloud/pouta/

## 2. Setting up cPouta VM, basics

Note. the best workflow and VM setup to be determined.

In general, follow this: https://docs.csc.fi/cloud/pouta/launch-vm-from-web-gui/ .

- I recommend installing Ubuntu from image.
- VM comes with some space, mostly for install system software (apt)
- Get a persistent volume and put all code,data there (one can move the volume between instances)

## 3. Accessing RStudio Server or Jupyter Notebooks running on cPouta via SSH tunneling

### Jupyter notebooks

(0. install jupyter notebooks and prerequisites. virtualenvs recommended.)
1. ssh into VM from your local machine
2. start a tmux screen (run `tmux` in bash)
3. navigate to the notebook directory and start a notebook server (`jupyter notebook`)
4. create an ssh tunnel in VM from your local machine that binds the VM port 8888 (used by notebook as default) e.g. to your local post 8888:
   `ssh -L 8888:localhost:8888 ubuntu@(floating IP of your VM)`
5. start a webbrowser on your local machine and go to localhost:8888.

### RStudio server

(0. install RStudio server)
(02. make sure you have created a user with password login on VM. terminal command `adduser`)
(03. after installation Rstudio server process should start automatically)
1. create an ssh tunnel in VM from your local machine that binds the VM port 8787 (used by rstudio as default) e.g. to your local post 8787:
2.    `ssh -L 8787:localhost:8787 ubuntu@(floating IP of your VM)`
3. start a webbrowser on your local machine and go to localhost:8787, log in with the username you have created with adduser
